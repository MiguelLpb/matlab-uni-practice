% Plot sine and cosine function
% myprogram.m

t = 0:.01:2;
y1 = sin(2*pi*t);
y2 = cos(2*pi*t);
plot(t,y1,t,y2), grid
xlabel('{\itt} \rightarrow'), ylabel('{\ity} ({\itt}) \rightarrow')
title('myprogram')