%Fourier-Reihe des Rechteckimpulszuges (Synthesegleichung)

function y = fourier(t,N)
% y = fourier(t,N)
% t : time samples
% N : number of harmonics to be used in partial sum (N>=1)
y = 0.5*ones(size(t)); %default amplitude = 0
for n = 0:N-1
    y = y + ((2/pi)/(2*n+1))*sin((2*n+1)*2*pi*t);
end
return