%Hauptprogramm Fourier-Synthese

%Fourier synthesis of rectangular impulse train
N = 10; %number of harmonics for partial sum
T = .01; % sampling interval (step size)
t = 0:T:3;
y = rectangular(t,1,.5);
yF = fourier(t,N);
plot(t,y,t,yF,'LineWidth',2), grid
axis([0 3 -.2 1.2])
xlabel('{\itt} \rightarrow')
ylabel('{\ity} ({\itt}), {\ity}_{F}({\itt}) \rightarrow')
TEXT = 'Fourier synthesis of rectangular impulse train';
title([TEXT, '(N=',num2str(N),')'])