%Fourier synthesis of sawtooth signal
%sawtooth_approx*mw*2017
Nh = 4; %number of harmonics
Nspp = 20; %samples per period
t = 0:1/Nspp:2; % normalized time (2 periods)
y = sawtooth(t,Nh); %compute signal
stem(t*Nspp,y,'filled'), grid
axis([0 2*Nspp+1 -1 1])
xlabel('{\itn} \rightarrow')
ylabel('y_{F} [{\itn}] \rightarrow')
TEXT = 'Fourier synthesis of sawtooth';
title([TEXT,' (N=',num2str(Nh),', Nspp=',num2str(Nspp),')'])
function y=sawtooth(t,Nh) %function in file available in R2016b
%Fourier approximation
%t : time samples (t>=0)
%Nh: num,ber of harmonics
y = zeros(size(t)); s = 1;
x = 2*pi*t;
for k = 1:Nh
    y = y + s*sin(k*x)/k;
    s = -1*s;
end
y = y*2/pi;
end
