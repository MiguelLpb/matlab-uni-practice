%Rechteckimpulszug

function y = rectangular(t,p,w)
% y = rectangular(t,p,w)
% t : time samples (t>=0)
% p : period
% w : impulse width
y = zeros(size(t)); %default amplitude = 0
for n = 1:length(t)
    x = mod(t(n), p); %mapping of t into fundamental period
    if x >= 0 && x <= w
        y(n) = 1; % set amplitude = 1
    end
end
return
