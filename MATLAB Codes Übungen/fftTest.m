% dft spectrum of several sequences
% fftTest.m 
N = 32; % length of sequences (period)
n = 0:N-1; % normalized time
A = 3; %Amplitude
Omega = 2*pi/32; % normalized radian frequency
x = A*cos(Omega*n); % cosine sequence
%x = sin(Omega*n); % sinus sequence
X = fft(x); % computation of dft spectrum
%X = fftshift(X);
% Graphics
FIG = figure('Name','dsp_dft_1','NumberTitle','off',...
  'Units','normal','Position',[.3 .5 .6 .4]);
subplot(2,2,1), stem(0:N-1,real(x),'filled'), grid
    MAX = max(abs(x)); axis([0 N-1 -MAX MAX]);
    xlabel('{\itn} \rightarrow')
    ylabel('Re( {\itx}[{\itn}] ) \rightarrow')
subplot(2,2,2), stem(0:N-1,imag(x),'filled'), grid
    axis([0 N-1 -1 1]);
    xlabel('{\itn} \rightarrow')
    ylabel('Im( {\itx}[{\itn}] ) \rightarrow')
subplot(2,2,3), stem(0:N-1,real(X),'filled'), grid
    MAX = max(abs(X)); axis([0 N-1 -MAX MAX]);
    xlabel('{\itk} \rightarrow')
    ylabel('Re( {\itX}[{\itk}] ) \rightarrow')
subplot(2,2,4), stem(0:N-1,imag(X),'filled'), grid
    axis([0 N-1 -MAX MAX]);
    xlabel('{\itk} \rightarrow')
    ylabel('Im( {\itX}[{\itk}] ) \rightarrow')