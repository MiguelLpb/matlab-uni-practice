%Fouriersynthese
T = 5; % Periodendauer in Sekunden
f = 1/T; % Frequenz des Signals
w = 2*pi*f; % w ist omega
% Dreieckskurve 7c
h = 3; % Amplitude des Rechtecks
k = 4*h/pi/pi; %eine Konstante vor den Koeffizienten
a0 = h/2; % Gleichanteil, eigentlich a0/2
% Cosinus-Anteile sind diesmal ungleich Null
a = [1 0 1/9 0 1/25 0 1/49 0 1/81 0 1/121 0 1/169 0 1/225 0 1/729];
% Sinus-Anteile sind diesmal Null
b = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]; 
% Synthese des periodischen Signals
t = -2*T:0.1:2*T; % 4 Perioden darstellen
x = 0*t; % Am Anfang ist alles Null
for ikk = 1:17 % wir haben 17 Summanden
    x = x + a(ikk)*k*cos(ikk*w*t) + b(ikk)*k*sin(ikk*w*t);
end
x = a0 + x; % Gleichanteil dazuaddieren, a0 ist hier eigentlich a0/2
plot(t,x)
xlabel('t in Sekunden')
ylabel('x(t)')
title('Dreieckskurve 7c')
grid