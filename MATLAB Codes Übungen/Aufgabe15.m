%Aufgabe15
n = 0:10;
h = [0.4 1 2 2 1.6 1.2 0.8 0.4 0.2 0 0];
subplot(2,1,1)
stem(n, h, 'LineWidth', 2)
grid
xlabel('n')
ylabel('h[n]')
h1 = cumsum(h)
subplot(2,1,2)
stem(n, h1, 'Linewidth', 2)
grid
xlabel('n')
ylabel('h_{-1}[n]')

