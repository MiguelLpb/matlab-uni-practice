%Aufgabe2d
t = [-2 -1 0 1 1 2 2 3 4];
x = [0 0 1 1 2 2 -1 0 0];
plot(t,x, 'LineWidth', 4)
grid
xlabel('t')
ylabel('x(t)')
title('Aufgabe2d')

% WENN AUFGABE: x(t)*[δ(t+1/2)-δ(t-3/2)] ist, dann
% x(-1/2) = 0,5 und x(1,5) = -2 EINFACH NACH KOORDINATEN GUCKEN
t = [-0.5 1.5];
x = [0.5 -2];
hold % die erste Graphik soll erhalten bleiben
stem(t,x, 'LineWidth', 2)

