% Übung 12 - Bodediagramm des Bandpasses 
R=50;
C=4.7e-6;
L=0.03;
%%Variante "zu Fuss"
w=100:10:1e5;
RC=R*C;
LC=L*C;
R_L=R/L;
H=j.*w.*RC./(1+j.*w.*RC-w.*w.*LC);
Habs=abs(H);
%Amplitudengang und Phasengang nichtnegativ wg. Kompatibilität zu Bode
a=20*log10(Habs); %Dämpfung in dB (Amplitudengang) nicht negativ angegeben!
b=atan(imag(H)./real(H))*360./2./pi;  % Phasenwinkel (Phasengang) nicht negativ angegeben

%%3db-Punkte w1 (-45°) und w2 (+45°) und w0 (0°)
w0=1/sqrt(LC) %Resonanz
w1=-R_L/2+sqrt(R_L^2/4+1/LC)  %untere Grenzfrequenz (phi=-45°)
w2=R_L/2+sqrt(R_L^2/4+1/LC)   %obere Grenzfrequenz (phi=+45°)
wpoints=[w1 w0 w2];
Hwpoints_=j.*wpoints.*RC./(1+j.*wpoints.*RC-wpoints.*wpoints.*LC);
awpoints=20*log10(abs(Hwpoints_)) % Dämpfung/log-Darstellung |H(jw)| für w0,w1,w2
bwpoints=angle(Hwpoints_)*360/2/pi % Phase H(jw) für w0,w1,w2
%plots
f1=figure;
f1.Position=[100 200 600 420];
subplot(2,1,1), semilogx(w,a)
hold on
semilogx(wpoints,awpoints,'*'),grid
axis([100 1e5 -40 0])
yticks([-30 -20 -10 -3 0])
title('Dämpfung und Phase - zu Fuß - incl. 3dB-Punkte und w0')
subplot(2,1,2), semilogx(w,b)
axis([100 1e5 -90 90])
hold on
semilogx(wpoints,bwpoints,'*')
yticks([-90 -45 0 45 90])
grid
% % Variante mit bode()
b0=0;
b1=R*C;
a0=1;
a1=R*C;
a2=L*C;
b=[b1 b0]
a=[a2 a1 a0]
HBP=tf(b,a)
f2=figure;
f2.Position=[750 200 600 420];
bode(HBP);
grid
