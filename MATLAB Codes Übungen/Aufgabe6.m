%Aufgabe6
n = -10:1:10;
x1 = rectpuls(n, 5);
x2 = rectpuls(n, 6);
x3 = rectpuls(n-3, 5);
x4 = rectpuls(n+4, 3);
x5 = tripuls(n, 5, 0);
x6 = tripuls(n, 5, 0.4);
subplot(3,2,1)
stem(n,x1)
title('Rect(n,5)')
subplot(3,2,2)
stem(n,x2)
title('Rect(n,6)')
subplot(3,2,3)
stem(n,x3)
title('Rect(n-3,5)')
subplot(3,2,4)
stem(n,x4)
title('Rect(n+4,3)')
subplot(3,2,5)
stem(n,x5)
title('Tri(n,3,0)')
subplot(3,2,6)
stem(n,x6)
title('Tri(n,3,0.4)')
