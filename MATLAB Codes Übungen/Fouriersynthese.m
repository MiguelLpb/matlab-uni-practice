%Fouriersynthese
T = 5; % Periodendauer in Sekunden
f = 1/T; % Frequenz des Signals
w = 2*pi*f; % w ist omega
% Rechteckkurve 1a
h = 3; % Amplitude des Rechtecks
k = 4*h/pi; %eine Konstante vor den Koeffizienten
a0 = 0; % Gleichanteil, das ist eigentlich a0/2
% Cosinus-Anteile sind hier alle Null
a = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
% Sinus-Anteile
b = [1 0 1/3 0 1/5 0 1/7 0 1/9 0 1/11 0 1/13 0 1/15 0 1/17]; 
% Synthese des periodischen Signals
t = -2*T:0.1:2*T; % 4 Perioden darstellen
x = 0*t; % Am Anfang ist alles Null
for ikk = 1:17 % wir haben 17 Summanden
    x = x + a(ikk)*k*cos(ikk*w*t) + b(ikk)*k*sin(ikk*w*t);
end
x = a0 + x; % Gleichanteil dazuaddieren, a0 ist hier eigentlich a0/2
plot(t,x)
xlabel('t in Sekunden')
ylabel('x(t)')
title('Rechteckkurve 1a')
grid