%Aufgabe9
n = -2:10;
x = [0 0 1 2 1 -1 -1 2 2 1 -1 0 0];
subplot(3,1,1)
stem(n, x)
xlabel('n')
ylabel('x[n]')
title('a) Eingangssignal')
grid
y = 0*n; % Am Anfang ist alles Null
for ikk = 2:12
    y(ikk) = (x(ikk-1) + x(ikk) + x(ikk+1))/3;
end
subplot(3,1,2)
stem(n,y)
xlabel('n')
ylabel('y[n]')
title('b) Ausgangssignal')
grid
h = [0 1/3 1/3 1/3 0 0 0 0 0 0 0 0 0];
subplot(3,1,3)
stem(n,h)
xlabel('n')
ylabel('h[n]')
title('c) Impulsantwort')
grid
