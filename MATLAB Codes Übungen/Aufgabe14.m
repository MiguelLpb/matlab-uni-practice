%Aufgabe14
%Faltung
xa = [1 1 1 1 1];
ha = [0 0 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1];
ya = conv(xa,ha);
n = 0:length(ya)-1;
subplot(3,1,1)
stem(n,ya)
xlabel('n')
ylabel('y[n]')
title('Aufgabe a)')
grid
xb = [1 2 1 1];
hb = [1 -1 0 0 1 1];
yb = conv(xb,hb);
n = -2:length(yb)-3;
subplot(3,1,2)
stem(n,yb)
xlabel('n')
ylabel('y[n]')
title('Aufgabe b)')
grid
xc = [0 1 1 0 1 1];
hc = [1 2 3 2 1];
yc = conv(xc,hc);
n = -4:length(yc)-5;
subplot(3,1,3)
stem(n,yc)
xlabel('n')
ylabel('y[n]')
title('Aufgabe c)')
grid
