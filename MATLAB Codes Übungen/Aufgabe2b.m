%Aufgabe2b
t = [-2 -1 0 1 1 2 2 3 4];
x = [0 0 1 1 2 2 -1 0 0];
plot(t,x, 'LineWidth', 4)
grid
xlabel('t')
ylabel('x(t)')
title('Aufgabe2b')

% WENN AUFGABE: x(1-t) ist, dann
% t-Werte mit -1 addieren
t = t - 1;
% anschließend negieren = an y-Achse spiegeln
t = -1.*t;
hold % die erste Graphik soll erhalten bleiben
plot(t,x, 'LineWidth', 2)

