%Aufgabe2c
t = [-2 -1 0 1 1 2 2 3 4];
x = [0 0 1 1 2 2 -1 0 0];
plot(t,x, 'LineWidth', 4)
grid
xlabel('t')
ylabel('x(t)')
title('Aufgabe2c')

% WENN AUFGABE: x(2t-2) ist, dann
% t-Werte mit 2 addieren
t = t + 2;
% t-Werte um Faktor 2 stauchen
t = t/2;
hold % die erste Graphik soll erhalten bleiben
plot(t,x, 'LineWidth', 2)
