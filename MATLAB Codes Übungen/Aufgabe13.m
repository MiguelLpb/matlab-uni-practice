%Aufgabe13
%Mittelwert mit Faltung
xa = [0 1 2 1 -1 -1 2 2 1 -1];
ha = [1/3 1/3 1/3];
ya = conv(xa,ha);
n = -1:length(ya)-2;
stem(n,ya)
xlabel('n')
ylabel('y[n]')
title('Gleitender Mittelwert')
grid
