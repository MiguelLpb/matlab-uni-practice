%Aufgabe1
A = 2.5; % Amplitude ist am Anfang 2,5cm
sigma = log(0.1/2.5)/20; % log ist ln in MATLAB
T = 20/3;
omega = 2*pi/T; 
s = sigma + j*omega;
t = 0:0.1:20;
x = A*exp(s*t);
plot(real(x), imag(x), "o")
xlabel('Realteil von x(t)')
ylabel('Imaginärteil von x(t)')
title('Allgemeine Exponentielle')
grid
