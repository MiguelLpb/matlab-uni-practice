%Aufgabe8b
T = 5; % Periodendauer in Sekunden
f = 1/T; % Frequenz des Signals
w = 2*pi*f; % w ist omega
% Sägezahnkurve 14E
h = 3; % Amplitude des Sägezahns
k = -h/pi;
a0 = h/2; % Gleichanteil
% Cosinus-Anteile, sind alle Null
a = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
% Sinus-Anteile
b = [1 1/2 1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10 1/11 1/12 1/13 1/14 1/15 1/16 1/17]; 
% Synthese des periodischen Signals
t = -2*T:0.1:2*T; % 4 Perioden darstellen
x = 0*t; % Am Anfang ist alles Null
for ikk = 1:17 % wir haben 17 Summanden
    x = x + a(ikk)*k*cos(ikk*w*t) + b(ikk)*k*sin(ikk*w*t);
end
x = a0 + x; % Gleichanteil dazuaddieren
plot(t,x)
xlabel('t in Sekunden')
ylabel('x(t)')
title('Sägezahnkurve 14E')
grid