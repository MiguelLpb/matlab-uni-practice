%Aufgabe4
n = 0:20;
xa = cos(pi/8*n);
xb = cos(4*pi/8*n);
xc = cos(17*pi/8*n);
xd = cos(3/8*n);
%subplot(2, 2, x) erzeugt 2 Diagramme nebeneinander und 2 untereinander
subplot(2,2,1)
stem(n,xa)
grid
title('a) Periode = 2\pi/\Omega = 16')
subplot(2,2,2)
stem(n,xb)
grid
title('b) Periode = 4')
subplot(2,2,3)
t = 0:0.01:20;
x = cos(17*pi/8*t);
plot(t,x)
hold
stem(n,xc)
grid
title('c) Periode = 16 wie bei a)')
subplot(2,2,4)
stem(n,xd)
grid
title('d) Periode = KEINE')
