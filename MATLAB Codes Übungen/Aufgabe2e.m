%Aufgabe2e
t = [-2 -1 0 1 1 2 2 3 4];
x = [0 0 1 1 2 2 -1 0 0];
plot(t,x, 'LineWidth', 4)
grid
xlabel('t')
ylabel('x(t)')
title('Aufgabe2e')

% WENN AUFGABE: x(2-t/3) ist, dann
% t-Werte mit 2 subtrahieren
t = t - 2;
% anschließend negieren = an y-Achse spiegeln
t = -1.*t;
% t-Werte um Faktor 3 dehnen
t = 3*t;
hold % die erste Graphik soll erhalten bleiben
plot(t,x, 'LineWidth', 2)