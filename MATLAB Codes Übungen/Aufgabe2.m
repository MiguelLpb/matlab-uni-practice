%Aufgabe2
%um senkrechte Linien zu plotten
%wird die t-Variable 2mal angegeben
t = [-2 -1 0 1 1 2 2 3 4];
x = [0 0 1 1 2 2 -1 0 0];
plot(t,x, 'LineWidth', 4)
grid
xlabel('t')
ylabel('x(t)')
title('Bild 1')
