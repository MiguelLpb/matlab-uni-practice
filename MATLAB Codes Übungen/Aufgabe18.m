%Aufgabe18
clear
A = 1;
R = 2000;
C = 0.001;
tau = R*C;
Te = 5*tau; %Ende-Zeit
Ts = 0.5; %Abtastintervall Ts
t = 0:Ts:Te;
B = length(t);
%Anlytische Lösung
ya = A*(1 - exp(-t/tau));
subplot(2,1,1)
plot(t, ya)
grid
xlabel('t / s')
ylabel('h_{-1}(t) / V')
title('Sprungantwort')
%diskrete Lösung
b_0 = Ts/(tau+Ts);
a_1 = -tau/(tau+Ts);
x(1) = A;
for ikk = 2:B
   x(ikk) = A;
end
y(1) = b_0*x(1); % ist y[0]
for ikk = 2:B
    y(ikk) = b_0*x(ikk) - a_1*y(ikk-1);
end
hold
stem(t, y)
%Impulsantwort
h(1) = y(1);
for ikk = 2:B
    h(ikk) = y(ikk) - y(ikk-1);
end
subplot(2,1,2)
stem(t, h)
grid
xlabel('t / s')
ylabel('h(t) / V')
title('Impulsantwort')
%Lösung mit fvtool(b, a)
b = [0.2 0]; %b_0 = 0.2 und b_1 = 1
a = [1 -0.8] %a_0 = 1 und a_1 = -0.8
fvtool(b, a)
