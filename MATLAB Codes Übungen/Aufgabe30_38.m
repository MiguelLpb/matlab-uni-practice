% dft spectrum of several sequences
% Loesungen zu Übung 9
%entsprechenden Lösungsteil einblenden, Anzeigeteil bleibt immer 
%%%%%%%%%%%%%%%%
%%A30
% clear  %Workspace löschen
% N=64
% n = 0:N-1; % normalized time
% k=32  % wird nach und nach erhöht
% Omega = k*2*pi/N
% x = cos(Omega*n)
%%%%%%%%%%%%%%%%
%%A31-32
% clear  %Workspace löschen
% N = 32; 
% n = 0:N-1; % normalized time
% A = 3; %Amplitude
% k1=1
% k2=4
% Omega1 = k1*2*pi/N; % normalized radian frequency
% Omega2 = k2*2*pi/N
% x = A*sin(Omega1*n)+6*sin(Omega2*n); % sine sequence
% %x = A*cos(Omega1*n)+6*cos(Omega2*n); % cosine sequence
%%%%%%%%%%%%%%%%%%%%%%%%
% A33 34 Deltatfunktion
% clear  %Workspace löschen
% N = 32;
% x=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]
%x=[0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]
%%%%%%%%%%%%%%%%%%%%%%%%%
%A35
% clear  %Workspace löschen
% N = 32;% 
% k=0  % k variieren
% omega=k*2*pi/N
% n = 0:N-1; % normalized time
% x=exp(i*omega*n)
%%%%%%%%%%%%%%%%%%%%%%%
%A36
% clear  %Workspace löschen
% N=32
%%Impulskam Abstand 4 (also 8 peaks)
% N=32
% x=[1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0]
%%Impulskam Abstand 8 (also 4 peaks)
% N=32
% x=[1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]
%%%%%%%%%%%%%%%%%%%%%%%
%A37
% clear  %Workspace löschen
% N=32
% x=[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
% n=-N/2:N/2-1
%x=heaviside(n)
%%%%%%%%%%%%%%%%%%%%%%%
%A38
% N=8
% x=[ 0 0 1 1 1 1 0 0 ]

%%%%%%%%%%%%%%%%%%%%%%
%%der Teil bleibt immer eingeblendet
X = fft(x); % computation of dft spectrum
X = fftshift(X);
% Graphics
FIG = figure('Name','dsp_dft_1','NumberTitle','off',...
  'Units','normal','Position',[.3 .5 .6 .4]);
subplot(2,2,1), stem(0:N-1,real(x),'filled'), grid
    MAX = max(abs(x)); axis([0 N-1 -MAX MAX]);
    xlabel('{\itn} \rightarrow')
    ylabel('Re( {\itx}[{\itn}] ) \rightarrow')
subplot(2,2,2), stem(0:N-1,imag(x),'filled'), grid
    axis([0 N-1 -1 1]);
    xlabel('{\itn} \rightarrow')
    ylabel('Im( {\itx}[{\itn}] ) \rightarrow')
subplot(2,2,3), stem(0:N-1,real(X),'filled'), grid
    MAX = max(abs(X)); axis([0 N-1 -MAX MAX]);
    xlabel('{\itk} \rightarrow')
    ylabel('Re( {\itX}[{\itk}] ) \rightarrow')
subplot(2,2,4), stem(0:N-1,imag(X),'filled'), grid
    axis([0 N-1 -MAX MAX]);
    xlabel('{\itk} \rightarrow')
    ylabel('Im( {\itX}[{\itk}] ) \rightarrow')